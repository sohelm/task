<?php
/**
 * Created by PhpStorm.
 * User: mindfire
 * Date: 18/6/18
 * Time: 11:47 AM
 */

namespace AppBundle\Admin;

use Sonata\AdminBundle\Admin\AbstractAdmin;
use Sonata\AdminBundle\Datagrid\ListMapper;
use Sonata\AdminBundle\Datagrid\DatagridMapper;
use Sonata\AdminBundle\Form\FormMapper;
use Symfony\Component\Form\Extension\Core\Type\TextType;

/**
 * Class operatorAdmin is used to set admin for Operator entity
 * @package AppBundle\Admin
 */
class operatorAdmin extends AbstractAdmin
{
    /**
     * This is used to list all operators by network id in descending order
     * @var array
     */
    protected $datagridValues = [

        // reverse order (default = 'ASC')
        '_sort_order' => 'DESC',

        // name of the ordered field (default = the model's id field, if any)
        '_sort_by' => 'networkId',
    ];

    /**
     * This is used to create custom query.We have used it to remove Distinct keyword form query as id is a primary key
     * @param string $context
     * @return \Sonata\AdminBundle\Datagrid\ProxyQueryInterface
     */
    public function createQuery($context = 'list')
    {

        $query = parent::createQuery($context);
        $query->setDistinct(false);
        return $query;
    }

    /**
     * Used to show form fields
     * @param FormMapper $formMapper
     */
    protected function configureFormFields(FormMapper $formMapper)
    {
        $formMapper->add('name', TextType::class);
        $formMapper->add('networkId', TextType::class);
    }

    /**
     * Used to implement filter option in dashboard
     * @param DatagridMapper $datagridMapper
     */
    protected function configureDatagridFilters(DatagridMapper $datagridMapper)
    {
        $datagridMapper->add('name');
    }

    /**
     * It adds fields to show in the list
     * @param ListMapper $listMapper
     */
    protected function configureListFields(ListMapper $listMapper)
    {
        $listMapper->addIdentifier('name'); // Adds edit link
        $listMapper->add('networkId');
    }
}