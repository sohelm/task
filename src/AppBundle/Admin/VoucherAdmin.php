<?php
/**
 * Created by PhpStorm.
 * User: mindfire
 * Date: 18/6/18
 * Time: 12:14 PM
 */

namespace AppBundle\Admin;

use AppBundle\Entity\Voucher;
use Sonata\AdminBundle\Admin\AbstractAdmin;
use Sonata\AdminBundle\Datagrid\ListMapper;
use Sonata\AdminBundle\Form\FormMapper;
use Sonata\CoreBundle\Form\Type\BooleanType;
use Symfony\Component\Form\Extension\Core\Type\DateTimeType;
use Symfony\Component\Form\Extension\Core\Type\NumberType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use AppBundle\Entity\Operator;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Sonata\AdminBundle\Datagrid\DatagridMapper;

/**
 * Class VoucherAdmin
 * @package AppBundle\Admin
 */
class VoucherAdmin extends AbstractAdmin
{
    /**
     * useed to show form fields in admin panel
     * @param FormMapper $formMapper
     */
    protected function configureFormFields(FormMapper $formMapper)
    {
        $formMapper
            ->with('Vouchers' , ['class' => 'col-md-9'])
                ->add('serialnumber', TextType::class)
                ->add('amount', NumberType::class)
                ->add('expiry', DateTimeType::class)
                ->add('state' , BooleanType::class)
            ->end()
            ->with('Mapping with Operators' , ['class' => 'col-md-3'])
                ->add('operator', EntityType::class, [
                    'class' => Operator::class,
                    'choice_label' => 'name',
                ])
            ->end()

        ;
    }

    
    public function toString($object)
    {
        return $object instanceof Voucher
            ? $object->getSerialnumber()
            : 'Voucher Entity'; // shown in the breadcrumb on the create view
    }

    /**
     * Used to implement filter option in dashboard
     * @param DatagridMapper $datagridMapper
     */
    protected function configureDatagridFilters(DatagridMapper $datagridMapper)
    {
        $datagridMapper
            ->add('serialnumber')
            ->add('operator', null, [], EntityType::class, [
                'class'    =>Operator::class,
                'choice_label' => 'name',
            ])
        ;
    }

    /**
     * It adds fields to show in the list
     * @param ListMapper $listMapper
     */
    protected function configureListFields(ListMapper $listMapper)
    {
        $listMapper
            ->addIdentifier('serialnumber')
            ->add('amount')
            ->add('expiry')
            ->add('state')
            ->add('operator.name')
        ;
    }

}