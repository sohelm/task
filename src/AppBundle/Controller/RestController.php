<?php
/**
 * Created by PhpStorm.
 * User: mindfire
 * Date: 12/6/18
 * Time: 4:35 PM
 */

namespace AppBundle\Controller;


use AppBundle\Entity\Operator;
use AppBundle\Services\SetOperator;
use FOS\RestBundle\Controller\FOSRestController;
use Psr\Log\LoggerInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use FOS\RestBundle\Controller\Annotations as Rest;
use FOS\RestBundle\View\View;

/**
 * This class is used to create Rest Api
 * Class RestController
 * @package AppBundle\Controller
 *
 */
class RestController extends FOSRestController
{

    /**
     * Used to set Operator name and network id where network id is unique
     * @Rest\Post("api/operator")
     *
     */
    public function setOperator(Request $request)
    {
        $valid = $this->checkValidation($request);
        if($valid == 1)
            return new View("Null value not accepted", Response::HTTP_NOT_ACCEPTABLE);
        else if( $valid == 2)
            return new View("Network Id already exist", Response::HTTP_NOT_ACCEPTABLE);
        $this->get('Set_Operator')->createOperator($request->get('name'), $request->get('netid'));
        
        return new View("Operator Created Successfully", Response::HTTP_OK);

    }

    /**
     * @Rest\Get("/api/operator")
     * Used to retrieve Operators
     */
    public function getOperatorAction()
    {
        $restresult = $this->get('Set_Operator')->getAllOperator();
        if (!$restresult) {
            return new View("there are no Operator exist", Response::HTTP_NOT_FOUND);
        }
        return $restresult;
    }
    /**
     * Used to retrieve operators by Id
     * @Rest\Get("/api/operator/{id}")
     *
     */
    public function getOperatorByIdAction($id)
    {
        $restresult = $this->get('Set_Operator')->getOperatorById($id);
        if (!$restresult) {
            return new View("there are no Operator exist with this id", Response::HTTP_NOT_FOUND);
        }
        return $restresult;
    }

    /**
     * @Rest\Get("/api/voucher/{jsondata}")
     *
     */
    public function getVoucherAction($jsondata){
        $result = $this->get('Set_Operator')->getVoucherData($jsondata);
        if( empty($result)){
            return new View("No vouchers are present" , Response::HTTP_NOT_FOUND);
        }
        return $result;
    }

    /**
     * Used to check log using monolog
     * @Rest\Get("/api/log")
     *
     */
    public function logAction(LoggerInterface $loggerInterface){
        $loggerInterface->info('I just got the logger');
        $loggerInterface->error('An error occurred');
        $loggerInterface->critical('I left the oven on!', array(
            'cause' => 'in_hurry',
        ));
        $this->get('monolog.logger.apiRequest')->info('It is Request log');
        $this->get('monolog.logger.apiResponse')->info('It is response log.');
       return 123;
    }
    
    public function checkValidation(Request $request){
        $valid =0 ;
        if(empty($request->get('name')) || empty($request->get('netid'))){
            $valid = 1;
        }
        $netidcheck = $this->getDoctrine()->getRepository('AppBundle:Operator')->getOpId($request->get('netid'));
       // dump($request->get('netid'));die;
        if ($netidcheck) {
            $valid = 2;

        }
        return $valid;
    }
}