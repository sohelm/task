<?php
/**
 * Created by PhpStorm.
 * User: mindfire
 * Date: 20/6/18
 * Time: 3:17 PM
 */

namespace AppBundle\Controller;

use FOS\UserBundle\Controller\SecurityController as BaseController;
use Symfony\Component\HttpFoundation\Request;


/**
 * This controller overrides SecuriryController from FosUserBundle and redirects to the home page if user is logged in
 * Class SecurityController
 * @package AppBundle\Controller
 *
 */
class SecurityController extends BaseController
{
    /**
     * @param Request $request
     * @return \Symfony\Component\HttpFoundation\RedirectResponse|\Symfony\Component\HttpFoundation\Response
     *
     */
    public function loginAction(Request $request)
    {

        $response = parent::loginAction($request);
        
        if($this->isGranted('IS_AUTHENTICATED_FULLY')){
            return $this->redirectToRoute('homepage');
        }
        return $response;

    }
}