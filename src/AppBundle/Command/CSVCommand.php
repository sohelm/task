<?php
/**
 * Created by PhpStorm.
 * User: mindfire
 * Date: 13/6/18
 * Time: 12:52 PM
 */

namespace AppBundle\Command;


use AppBundle\Services\CSVService;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputDefinition;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Finder\Finder;
use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;

class CSVCommand extends ContainerAwareCommand
{
    /**
     * @var array
     */
    private $csvParsingOptions = array(
        'finder_in' => 'app/Resources/',
        'finder_name' => 'fixtures.csv',
        'ignoreFirstLine' => true
    );

    /**
     *This method creates command and takes value from command line
     */
    protected function configure()
    {
        $this->setName('app:CSVUpload')
            ->setDescription('Upload CSV file having serial no.')
            ->setHelp("This command allows you upload csv files in voucher table ")
            ->addArgument('filename' , InputArgument::REQUIRED , 'text to print')
            ->addArgument('network_id' , InputArgument::REQUIRED , 'network id')
            ->addArgument('expiry' , InputArgument::REQUIRED, 'Expiry date')

        ;
    }

    /**
     * This method executes the CSVUpload command and calls CSVService
     * @param InputInterface $input
     * @param OutputInterface $output
     * @return int|null|void
     */
    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $filename = $input->getArgument('filename');
        $network_id = $input->getArgument('network_id');
        $expiry = $input->getArgument('expiry');
        $this->csvParsingOptions['finder_name']=$filename;
        $output->writeln("File name : ".$filename);
        $output->writeln("network_id : ".$network_id);
        $output->writeln("Expiry: ". $expiry);
        $csv = $this->parseCSV();
        $res = $this->getContainer()->get('CSV_Upload');
        $doctrine = $this->getContainer()->get('doctrine');
        $em = $doctrine->getEntityManager();
        $res = new CSVService($em);
        $message = $res->getCSVFile($csv , $network_id,$expiry);
        $output->writeln("==============");
        $output->writeln($message);


    }

    /**
     * 
     * @return array
     * Used to parse CSV file
     */
    private function parseCSV()
    {
        $ignoreFirstLine = $this->csvParsingOptions['ignoreFirstLine'];

        $finder = new Finder();
        $finder->files()
            ->in($this->csvParsingOptions['finder_in'])
            ->name($this->csvParsingOptions['finder_name'])
        ;
        foreach ($finder as $file) { $csv = $file; }

        $rows = array();
        if (($handle = fopen($csv->getRealPath(), "r")) !== FALSE) {
            $i = 0;
            while (($data = fgetcsv($handle, null, ";")) !== FALSE) {
                $i++;
                if ($ignoreFirstLine && $i == 1) { continue; }
                $rows[] = $data;
            }
            fclose($handle);
        }

        return $rows;
    }

}