<?php
/**
 * Created by PhpStorm.
 * User: mindfire
 * Date: 13/6/18
 * Time: 2:40 PM
 */

namespace AppBundle\Services;


use AppBundle\Entity\Voucher;
use Doctrine\ORM\EntityManager;
use Symfony\Component\Config\Definition\Exception\Exception;

class CSVService
{
    /**
     * @var EntityManager
     */
    private $em;

    /**
     * CSVService constructor.
     * @param EntityManager $em
     */
    public function __construct(EntityManager $em)
    {
        $this->em = $em;
    }

    /**
     * It is used to store data to voucher table coming from csv file  
     * @param $filename
     * @param $networkId
     * @param $expiry
     * @return string
     * Used to retrieve data from csv file and put it in database
     */
    public function getCSVFile($filename , $networkId , $expiry){
        $res = $this->em->getRepository('AppBundle:Voucher')->getOprtr($networkId);
        if(!$res){
            return "Network Id does not exist" ;
        }
        foreach ($filename as $serial) {
            $voucher = new Voucher();
            $voucher->setAmount(rand(10, 1000));
            $voucher->setExpiry(new \DateTime($expiry));
            $voucher->setOperator($res[0]->getOperator());
            $voucher->setSerialnumber($serial[0]);
            $voucher->setState(false);
            $this->em->getConnection()->beginTransaction();
            $this->em->persist($voucher);

            try {
                $this->em->flush();
                $this->em->getConnection()->commit();
            } catch (Exception $e) {
                $this->em->getConnection()->rollBack();
                $this->em->close();
                throw $e;
            }
        }
        return "Data added Successfully";

    }
}