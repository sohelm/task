<?php
/**
 * Created by PhpStorm.
 * User: mindfire
 * Date: 19/6/18
 * Time: 12:39 PM
 */

namespace AppBundle\Services;


use AppBundle\Entity\Operator;
use Doctrine\ORM\EntityManager;
use Doctrine\ORM\EntityManagerInterface;

/**
 * Class OperatorService
 * @package AppBundle\Services
 * This service is used to do all the doctrine like setting operator getting values
 */
class OperatorService
{
    /**
     * @var EntityManager
     */
    private $entityManager;
    /**
     * SetOperator constructor.
     * @param EntityManager $entityManagerInterface
     * This constructor is used for dependency injection and returns Entity manager object
     */
    public function __construct(EntityManager $entityManager)
    {
        $this->entityManager = $entityManager;
    }

    /**
     * @param $name
     * @param $netid
     * This takes arguments form RestController->setOperator() and sets it to the database
     */
    public function createOperator($name , $netid){
        $operator = new Operator();
        $operator->setName($name);
        $operator->setNetworkId($netid);
        $operator->setCreatedAt(new \DateTime('now'));
        $this->entityManager->persist($operator);
        $this->entityManager->flush();
        
    }

    /**
     * @return \AppBundle\Entity\Operator[]|array
     * Returns all the operators
     * calls from - RestOperator->getOperator
     */
    public function getAllOperator(){
        $resultset = $this->entityManager->getRepository('AppBundle:Operator')->findAll();

        return $resultset;
    }

    /**
     * @param $id
     * @return Operator
     * This returns operator matching with id
     * calls from - RestOperator->getOperatorById
     */
    public function getOperatorById($id){
        $resultset = $this->entityManager->getRepository('AppBundle:Operator')->find($id);
        
        return $resultset;
    }

    /**
     * @param $data
     * @return mixed
     * This returs all the voucher whis matches to amount
     * call -getVoucher from voucher repository
     * calls from - RestOperator->getVoucher
     */
    public function getVoucherData($data){
        $data = json_decode($data , true);
        $net_id = $data['net_id'];
        $amount = $data['amount'];
        $quantity = $data['quantity'];
        $result = $this->entityManager->getRepository('AppBundle:Voucher')->getVoucher($net_id , $amount ,$quantity);
        return $result;
    }
}