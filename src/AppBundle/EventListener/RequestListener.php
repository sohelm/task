<?php
/**
 * Created by PhpStorm.
 * User: mindfire
 * Date: 19/6/18
 * Time: 10:32 AM
 */

namespace AppBundle\EventListener;


use Monolog\Logger;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Event\GetResponseEvent;
class RequestListener
{

    /**
     * @var Logger
     */
    private $logger;
    private $api_token;
    /**
     * RequestListener constructor.
     * @param Logger $logger
     */
    public function __construct(Logger $logger , $api_token)
    {
        $this->logger = $logger;
        $this->api_token = $api_token;
    }

    /**
     * @param GetResponseEvent $event
     * This is used to authenticate API and store request log
     */
    public function onKernelRequest(GetResponseEvent $event){
        $api = $event->getRequest()->headers->get('AUTH');
        $request = $event->getRequest()->getRequestUri();
      /*  if($api != $this->api_token){
            $response = new Response();
            $response->setStatusCode(401);
            $response->setContent('Not authenticated');
            $event->setResponse($response);
        }
        else{
            $ev = $event->getRequest()->getUri();
            $this->logger->info($ev.'');
        }*/
        if(preg_match('/api/' , $request)){
            $this->logger->info($request.'');
        }
    }
}