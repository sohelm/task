<?php
/**
 * Created by PhpStorm.
 * User: mindfire
 * Date: 19/6/18
 * Time: 11:02 AM
 */

namespace AppBundle\EventListener;


use Monolog\Logger;
use Symfony\Component\HttpKernel\Event\FilterResponseEvent;

class ResponseListener
{
    private $logger;
    private $sonataLogger;
    /**
     * ResponseListener constructor.
     * @param Logger $logger
     */
    public function __construct(Logger $logger , Logger $sonataLogger)
    {
        $this->logger = $logger;
        $this->sonataLogger = $sonataLogger;
    }

    /**
<<<<<<< HEAD
     * It logs all the requests and responses
     * @param FilterResponseEvent $event
=======
     * @param FilterResponseEvent $event
     * This is used to store request as well as response log
>>>>>>> CodeOptimize
     */
    public function onKernelResponse(FilterResponseEvent $event){
        $request = $event->getRequest();
        $api = $request->headers->get('AUTH');
        $response = $event->getResponse();
        if(isset($api)) {
            $this->logger->info("Responce for " . $request->getRequestUri() . " is \n" . $response->getContent() . '');
        }
        if(preg_match('/admin/' ,$request)){
            $this->sonataLogger->debug($event->getResponse()->getContent());
        }
       
    }

}