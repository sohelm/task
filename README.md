About project
=======================

This project is created to learn symfony3.4 . I have learned to create RestApis and to authenticate them.
I have also learned about FosUserBundle and SonataBundle  and overridden them.

Pre Requirements
=======================
* Ubuntu Server 16.04

* Apache 2.4.*

* MySQL 5.7.*

* PHP 7.*

Installation
=======================
* git clone https://sohelm@bitbucket.org/sohelm/task.git

* composer install
* php bin/console assets:install
* php bin/console cache:clear
* php bin/console doctrine:database:create
* php bin/console doctrine:migrations:diff
* php bin/console doctrine:migrations:migrate

What's inside?
--------------

The Symfony Standard Edition is configured with the following defaults:

  * An AppBundle you can use to start coding;

  * Twig as the only configured template engine;

  * Doctrine ORM/DBAL;

  * Swiftmailer;

  * Annotations enabled for everything.

Bundles Used:
-----------------------
  * **FrameworkBundle** - The core Symfony framework bundle

  * [**SensioFrameworkExtraBundle**][6] - Adds several enhancements, including
    template and routing annotation capability

  * [**DoctrineBundle**] - Adds support for the Doctrine ORM

  * [**TwigBundle**] - Adds support for the Twig templating engine

  * [**SecurityBundle**] - Adds security by integrating Symfony's security
    component

  * [**SwiftmailerBundle**] - Adds support for Swiftmailer, a library for
    sending emails

  * [**MonologBundle**] - Adds support for Monolog, a logging library

  * **WebProfilerBundle** (in dev/test env) - Adds profiling functionality and
    the web debug toolbar

  * **SensioDistributionBundle** (in dev/test env) - Adds functionality for
    configuring and working with Symfony distributions

  * [**SensioGeneratorBundle**] (in dev env) - Adds code generation
    capabilities

  * [**WebServerBundle**] (in dev env) - Adds commands for running applications
    using the PHP built-in web server

  * **DebugBundle** (in dev/test env) - Adds Debug and VarDumper component
    integration
  * **migrations** (in dev/test env) - offers you the ability to pragmatically deploy new versions of your database
                                       schema in a safe, easy and standardized way.
  * **FOSRestBundle** - This bundle is just a tool to help you in the job of creating a REST API with Symfony

  * **SerializerBundle**  - This bundle is for serializing objects

  * **NelmioCorsBundle** - This bundle  allows to send cross origin requests

  * **SonataBundle** - This bundle is used to generate robust user friendly administration interfaces.

  * **FOSUserBundle** - for user management


Enjoy!


